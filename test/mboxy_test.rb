require "test_helper"

class MessageParser
  attr_reader :recipient, :boundary
  attr_reader :date_sent, :subject, :content
  attr_reader :messages

  def initialize(folder, file)
    @folder = folder
    @file = file
    @messages = []
  end
  
  def parse
    File.open("#{@folder}/#{@file}").each_line do |line|
      if line.include?('To:')
        @recipient = parse_value(line)
      end
  
      if line.include?('Date:')
        @date_sent = parse_value(line)  
      end
      
      if line.include?('Subject:')
        @subject = parse_value(line)  
      end
      
      if line.include?('boundary')
        @boundary = parse_boundary(line)
        @content = parse_content
        
        message = {}
        
        message[:recipient] = @recipient
        message[:date_sent] = @date_sent
        message[:subject] = @subject
        message[:content] = @content
        message[:boundary] = @boundary

        @messages << message
      end
    end
  end
    
  private
  
  def parse_value(line)
    line.split(':')[1].strip
  end
  
  def parse_content
    message_start = "--#{boundary}"
    content = ''
    
    file = File.open("#{@folder}/#{@file}")

    message_begins = false

    file.each_line do |line|
       if line.include?(message_start)
         message_begins = true
       end
       if message_begins
         if line.include?('Content-Type: text/html;')
           break
         else
           unless line.include?(message_start)
             unless line.include?('Content-Type: text/plain;') 
               content << line 
             end
           end
         end
       end   
    end
    content
  end  
  
  def parse_boundary(line)
    line.split('boundary')[1].gsub(/[^0-9a-z ]/i, '')
  end
end
  
class MboxyTest < Minitest::Test  
  def setup
    @parser = MessageParser.new('./test/fixtures', 'sample.mbox')
    @parser.parse
    @message = @parser.messages.first
  end
  
  def test_parse_recipient    
    assert_equal 'christophe.karcher@gmail.com', @message[:recipient]
  end
  
  def test_parse_date
    assert_equal 'Wed, 13 Mar 2019 17', @message[:date_sent]
  end

  def test_parse_subject
    assert_equal 'Quick Question', @message[:subject]
  end

  def test_parse_content
    assert @message[:content].size > 0
  end

  def test_parse_boundary
    assert_equal "000000000000d7748d0584018ed0", @message[:boundary]
  end

  def test_parse_all_messages
    assert_equal 4, @parser.messages.size
  end
  
end


