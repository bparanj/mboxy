class MessageParser
  HTML_EMAIL_CONTENT = 'Content-Type: text/html;'
  TEXT_EMAIL_CONTENT = 'Content-Type: text/plain;'
  DATE_SENT = 'Date:'
  RECIPIENT = 'To:'
  SUBJECT = 'Subject:'
  BOUNDARY = 'boundary'
  
  attr_reader :recipient, :date_sent, :subject, :content
  attr_reader :boundary

  def initialize(folder, file)
    @folder = folder
    @file = file
  end
  
  def parse
    File.open("#{@folder}/#{@file}").each_line do |line|
      if line.include?(RECIPIENT)
        @recipient = parse_value(line)
      end
  
      if line.include?(DATE_SENT)
        @date_sent = parse_value(line)  
      end
      
      if line.include?(SUBJECT)
        @subject = parse_value(line)  
      end
      
      if line.include?(BOUNDARY)
        @boundary = parse_boundary(line)
        @content = parse_content
        
        message = create_message
        yield message
      end
    end
  end
    
  private
    
  def parse_value(line)
    line.split(':')[1].strip
  end  
  
  def parse_boundary(line)
    line.split(BOUNDARY)[1].gsub(/[^0-9a-z ]/i, '')
  end
  
  def parse_content
    message_start = "--#{boundary}"
    content = ''
    message_begins = false
        
    file = File.open("#{@folder}/#{@file}")

    file.each_line do |line|
     message_begins = true if line.include?(message_start)
       
     if message_begins
       break if line.include?(HTML_EMAIL_CONTENT)
         
       unless line.include?(message_start)
         unless line.include?(TEXT_EMAIL_CONTENT) 
           content << line 
         end
       end       
     end   
    end
    content
  end
  
  def create_message
    message = {}
    
    message[:recipient] = recipient
    message[:date_sent] = date_sent
    message[:subject] = subject
    message[:content] = content
    message[:boundary] = boundary
    message
  end
  
end